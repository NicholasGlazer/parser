try {
    var Spooky = require('spooky');
} catch (e) {
    var Spooky = require('./node_modules/spooky');
}

var spooky = new Spooky({
    child: {
        transport: 'http'
    },
    casper: {
        pageSettings: {
            loadImages: false,//The script is much faster when this field is set to false
            loadPlugins: false,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
        },
        logLevel: "info", // Only "info" level messages will be logged
        verbose: true // log messages will be printed out to the console 
        
    }
}, function (err) {
        var startURL = 'https://www.xplace.com/ShowLogin.xpl';
        var projectURL = 'https://www.xplace.com/ShowProject.xpl?pj=88940';

        if (err) {
            e = new Error('Failed to initialize SpookyJS');
            e.details = err;
            throw e;
        }

        //First step is to open Xplace
        spooky.start().thenOpen(startURL, function() {
            console.log("Xplace website opened");
        });

        //Now we have to populate username and password, and submit the form
        spooky.then(function(){
            console.log("Login using username and password");
            this.evaluate(function(){
                document.getElementById("username").value="eternitech";
                document.getElementById("password").value="salbag";
                document.querySelector('form[name="LoginForm"]').submit();
            });
        });

        //Next to find selectors and write to json file;
        spooky.thenOpen(projectURL, function() {
            console.log('Open selected page and scrap all the content inside');
            var fs = require('fs');
            var utils = require('utils');
            var output = './result.json';

            //data to write
            data = {};
            data.project_title = this.fetchText('#project_name>span');
            data.project_desc = this.fetchText('div#project_desc');
            data.client_name = this.fetchText('.client .name a>span');
            data.categories = this.fetchText('.cats .value span a');
            data.client_total_projects = this.fetchText('.client .jobs', false);
            var file = JSON.stringify(data, null, 4);
            //write file 
            fs.write(output, file, 'w'); 
            console.log('output: ' + file);
            utils.dump(JSON.stringify(data, null, 4));
            console.log(utils.dump(JSON.stringify(data, null, 4)));

        });

        spooky.run();
});

// If you want to store cookies in CasperJS, you can use the PhantomJS cookiejar, which is accesible natively through CasperJS.
// Simply add following option when you launch CasperJS: --cookies-file=cookies.txt